From: Ondrej Jirman <megous@megous.com>
Date: Fri, 28 Jan 2022 16:57:31 +0100
Subject: power: supply: rk818: Don't ever set input current limit to 0

All USB power sources will allow the at least 500 mA to be consumed.
Without this patch, when fusb302 updates its psy after boot, without
properly runnning the usb2 phy based charger type detection, at least
we don't drop the input current limit to 0, preventing charging
completely.

Signed-off-by: Ondrej Jirman <megous@megous.com>
---
 drivers/power/supply/power_supply_core.c | 10 +++++++++-
 drivers/power/supply/rk818_charger.c     |  2 +-
 include/linux/power_supply.h             |  2 ++
 3 files changed, 12 insertions(+), 2 deletions(-)

diff --git a/drivers/power/supply/power_supply_core.c b/drivers/power/supply/power_supply_core.c
index 2abd4d3..91aa09e 100644
--- a/drivers/power/supply/power_supply_core.c
+++ b/drivers/power/supply/power_supply_core.c
@@ -376,7 +376,7 @@ int power_supply_is_system_supplied(void)
 }
 EXPORT_SYMBOL_GPL(power_supply_is_system_supplied);
 
-int power_supply_set_input_current_limit_from_supplier(struct power_supply *psy)
+int power_supply_set_input_current_limit_from_supplier_min(struct power_supply *psy, int minimum)
 {
 	union power_supply_propval val = {0,};
 	struct class_dev_iter iter;
@@ -405,9 +405,17 @@ int power_supply_set_input_current_limit_from_supplier(struct power_supply *psy)
 	if (ret)
 		return ret;
 
+	val.intval = max(val.intval, minimum);
+
 	return psy->desc->set_property(psy,
 				POWER_SUPPLY_PROP_INPUT_CURRENT_LIMIT, &val);
 }
+EXPORT_SYMBOL_GPL(power_supply_set_input_current_limit_from_supplier_min);
+
+int power_supply_set_input_current_limit_from_supplier(struct power_supply *psy)
+{
+	return power_supply_set_input_current_limit_from_supplier_min(psy, 0);
+}
 EXPORT_SYMBOL_GPL(power_supply_set_input_current_limit_from_supplier);
 
 int power_supply_set_battery_charged(struct power_supply *psy)
diff --git a/drivers/power/supply/rk818_charger.c b/drivers/power/supply/rk818_charger.c
index 7b67a0b..ba27932 100644
--- a/drivers/power/supply/rk818_charger.c
+++ b/drivers/power/supply/rk818_charger.c
@@ -234,7 +234,7 @@ static void rk818_usb_power_external_power_changed(struct power_supply *psy)
 {
         struct rk818_charger *cg = power_supply_get_drvdata(psy);
 
-	power_supply_set_input_current_limit_from_supplier(cg->usb_psy);
+	power_supply_set_input_current_limit_from_supplier_min(cg->usb_psy, 500000);
 }
 
 static enum power_supply_property rk818_usb_power_props[] = {
diff --git a/include/linux/power_supply.h b/include/linux/power_supply.h
index e218041..d0670c6 100644
--- a/include/linux/power_supply.h
+++ b/include/linux/power_supply.h
@@ -599,6 +599,8 @@ extern void power_supply_changed(struct power_supply *psy);
 extern int power_supply_am_i_supplied(struct power_supply *psy);
 extern int power_supply_set_input_current_limit_from_supplier(
 					 struct power_supply *psy);
+extern int power_supply_set_input_current_limit_from_supplier_min(
+					 struct power_supply *psy, int minimum);
 extern int power_supply_set_battery_charged(struct power_supply *psy);
 
 #ifdef CONFIG_POWER_SUPPLY
