From: Elias Rudberg <mail@eliasrudberg.se>
Date: Tue, 29 Jun 2021 14:42:19 +0200
Subject: usb: typec: tps6598x: Change power status check in
 tps6598x_interrupt

Since power status can change not only when
TPS_REG_INT_POWER_STATUS_UPDATE is set but also when
TPS_REG_INT_PP_SWITCH_CHANGED is set, check for power
status change also in the TPS_REG_INT_PP_SWITCH_CHANGED
case. In this way we detect power status change in some
cases where it was earlier undetected.

Fixes issues 250 and 291

Signed-off-by: Elias Rudberg <mail@eliasrudberg.se>
---
 drivers/usb/typec/tipd/core.c | 9 ++++++++-
 1 file changed, 8 insertions(+), 1 deletion(-)

diff --git a/drivers/usb/typec/tipd/core.c b/drivers/usb/typec/tipd/core.c
index 06b5f85..a1f82cf 100644
--- a/drivers/usb/typec/tipd/core.c
+++ b/drivers/usb/typec/tipd/core.c
@@ -724,7 +724,14 @@ static irqreturn_t tps6598x_interrupt(int irq, void *data)
 	if (!tps6598x_read_status(tps, &status))
 		goto err_clear_ints;
 
-	if ((event1 | event2) & TPS_REG_INT_POWER_STATUS_UPDATE) {
+	/*
+	 * In practice it seems like pwr_status can change also if the
+	 * TPS_REG_INT_PP_SWITCH_CHANGED bit is set, so we interpret
+	 * either of the TPS_REG_INT_POWER_STATUS_UPDATE or
+	 * TPS_REG_INT_PP_SWITCH_CHANGED bits being set as a possible
+	 * power status change.
+	 */
+	if ((event1 | event2) & (TPS_REG_INT_POWER_STATUS_UPDATE | TPS_REG_INT_PP_SWITCH_CHANGED)) {
 		if (!tps6598x_read_power_status(tps))
 			goto err_clear_ints;
 
