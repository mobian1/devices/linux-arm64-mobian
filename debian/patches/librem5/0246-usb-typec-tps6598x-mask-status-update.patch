From: Angus Ainslie <angus@akkea.ca>
Date: Fri, 1 Jan 2021 17:11:42 +0100
Subject: usb: typec: tps6598x: mask status update

We don't need the status update unless it's plugged in.

With broken CC lines the cable sometimes shows as plugged in even when it
isn't. Look further into the status register to see if it is connected and
disable the STATUS interrupt to stop the irq storm.

Signed-off-by: Angus Ainslie <angus@akkea.ca>
---
 drivers/usb/typec/tipd/core.c | 69 +++++++++++++++++++++++++++++++++++++++++++
 1 file changed, 69 insertions(+)

diff --git a/drivers/usb/typec/tipd/core.c b/drivers/usb/typec/tipd/core.c
index 7710349..06b5f85 100644
--- a/drivers/usb/typec/tipd/core.c
+++ b/drivers/usb/typec/tipd/core.c
@@ -48,6 +48,8 @@
 #define TPS_TYPEC_3000mA 3000000
 #define TPS_USB_5V	 5000000
 
+#define CC_INT_MASK			TPS_REG_INT_STATUS_UPDATE
+
 /* TPS_REG_SYSTEM_CONF bits */
 #define TPS_SYSCONF_PORTINFO(c)		((c) & 7)
 
@@ -305,6 +307,46 @@ static void tps6598x_debugfs_exit(const struct tps6598x *tps) { }
 
 #endif
 
+static int tps6598x_mask_reg(struct tps6598x *tps, int reg, u64 mask, bool set)
+{
+	u64 val;
+	int ret;
+
+	ret = tps6598x_read64(tps, reg, &val);
+	if (ret < 0) {
+		dev_err(tps->dev, "Reading reg 0x%x mask failed %d", reg, ret);
+		return ret;
+	}
+	if (set)
+		val |= mask;
+	else
+		val &= ~mask;
+	ret = tps6598x_write64(tps, reg, val);
+	if (ret < 0) {
+		dev_err(tps->dev, "Writing reg 0x%x mask failed %d", reg, ret);
+		return ret;
+	}
+
+	dev_dbg(tps->dev, "register mask updated %llx %llx", val, mask);
+
+	return 0;
+}
+
+static int tps6598x_mask_cc_int(struct tps6598x *tps, bool disable)
+{
+	int ret;
+
+	ret = tps6598x_mask_reg(tps, TPS_REG_INT_MASK1, CC_INT_MASK, !disable);
+	ret |= tps6598x_mask_reg(tps, TPS_REG_INT_MASK2, CC_INT_MASK, !disable);
+
+	if (ret < 0) {
+		dev_err(tps->dev, "Writing interrupt mask failed %d", ret);
+		return ret;
+	}
+
+	return 0;
+}
+
 static int tps6598x_connect(struct tps6598x *tps, u32 status)
 {
 	struct typec_partner_desc desc;
@@ -343,6 +385,8 @@ static int tps6598x_connect(struct tps6598x *tps, u32 status)
 	if (desc.identity)
 		typec_partner_set_identity(tps->partner);
 
+	tps6598x_mask_cc_int(tps, false);
+
 	power_supply_changed(tps->psy);
 
 	return 0;
@@ -379,6 +423,7 @@ static void tps6598x_disconnect(struct tps6598x *tps, u32 status)
 
 	memset(&tps->terms, 0, sizeof(struct tps6598x_pdo));
 
+	tps6598x_mask_cc_int(tps, true);
 	power_supply_changed(tps->psy);
 }
 
@@ -661,6 +706,7 @@ static irqreturn_t tps6598x_interrupt(int irq, void *data)
 	u32 status;
 	bool psy_changed = false;
 	int ret;
+	u64 mask;
 
 	mutex_lock(&tps->lock);
 
@@ -709,6 +755,27 @@ static irqreturn_t tps6598x_interrupt(int irq, void *data)
 		psy_changed = true;
 	}
 
+	if ((event1 | event2) & TPS_REG_INT_STATUS_UPDATE) {
+		ret = tps6598x_read64(tps, TPS_REG_INT_MASK1, &mask);
+		if (ret < 0)
+			dev_err( tps->dev, "Reading interrupt mask failed");
+		dev_dbg(tps->dev, "Status update: %x %llx", status, mask);
+		if (!(mask & TPS_REG_INT_STATUS_UPDATE))
+			dev_err( tps->dev, "The interrupt is masked , how did it fire ?? %llx", mask);
+
+		if (!(status & TPS_STATUS_PLUG_PRESENT) ||
+		    TPS_STATUS_CONN_STATE(status) !=
+		    (TPS_STATUS_CONN_STATE_CONN_NO_R_A | TPS_STATUS_CONN_STATE_CONN_WITH_R_A)) {
+			/* the status update register can fire even when masked so try
+			   and mask it again */
+			ret = tps6598x_mask_cc_int(tps, true);
+			if (ret < 0)
+				dev_err( tps->dev, "Writing interrupt mask failed");
+			else
+				dev_dbg( tps->dev, "interrupt mask updated %llx", mask);
+		}
+	}
+
 err_clear_ints:
 	tps6598x_write64(tps, TPS_REG_INT_CLEAR1, event1);
 	tps6598x_write64(tps, TPS_REG_INT_CLEAR2, event2);
@@ -1067,6 +1134,8 @@ static int tps6598x_probe(struct i2c_client *client)
 		ret = tps6598x_connect(tps, status);
 		if (ret)
 			dev_err(&client->dev, "failed to register partner\n");
+	} else {
+		tps6598x_mask_cc_int(tps, true);
 	}
 
 	ret = devm_request_threaded_irq(&client->dev, client->irq, NULL,
