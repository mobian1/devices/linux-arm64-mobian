From: =?utf-8?q?Guido_G=C3=BCnther?= <agx@sigxcpu.org>
Date: Tue, 16 Nov 2021 12:11:06 +0100
Subject: drm/edid: Allow to add checksum quirks
MIME-Version: 1.0
Content-Type: text/plain; charset="utf-8"
Content-Transfer-Encoding: 8bit

There are devices out there that have a broken checksum but otherwise
report correct edid. Allow to add quirks for those as well.

Signed-off-by: Guido Günther <guido.gunther@puri.sm>
---
 drivers/gpu/drm/drm_edid.c | 49 ++++++++++++++++++++++++++++++++++++++++++++++
 1 file changed, 49 insertions(+)

diff --git a/drivers/gpu/drm/drm_edid.c b/drivers/gpu/drm/drm_edid.c
index 83e5c11..a872d63 100644
--- a/drivers/gpu/drm/drm_edid.c
+++ b/drivers/gpu/drm/drm_edid.c
@@ -1655,6 +1655,52 @@ bool drm_edid_are_equal(const struct edid *edid1, const struct edid *edid2)
 }
 EXPORT_SYMBOL(drm_edid_are_equal);
 
+
+static void edid_vendor_string(const struct edid *edid, char vendor[4])
+{
+	vendor[0] = ((edid->mfg_id[0] & 0x7c) >> 2) + '@';
+	vendor[1] = (((edid->mfg_id[0] & 0x3) << 3) |
+			  ((edid->mfg_id[1] & 0xe0) >> 5)) + '@';
+	vendor[2] = (edid->mfg_id[1] & 0x1f) + '@';
+}
+
+
+static const struct edid_checksum_quirk {
+	char vendor[4];
+	int product_id;
+	u32 block;
+	u32 csum;
+} edid_checksum_quirk_list[] = {
+	/* NexDock Touch (NDK2014) */
+	{ "RTK", 0x2a3b, 0, 65 },
+};
+
+static bool edid_vendor(const struct edid *edid, const char *vendor);
+
+static bool
+checksum_quirk(const struct edid *edid, u32 block, u32 csum)
+{
+	const struct edid_checksum_quirk *quirk;
+	char vendor[4];
+	int i;
+
+	edid_vendor_string (edid, vendor);
+	for (i = 0; i < ARRAY_SIZE(edid_checksum_quirk_list); i++) {
+		quirk = &edid_checksum_quirk_list[i];
+		if (edid_vendor(edid, quirk->vendor) &&
+		    (EDID_PRODUCT_ID(edid) == quirk->product_id) &&
+		    block == quirk->block && csum == quirk->csum) {
+			DRM_DEBUG("Found checksum quirk for %s 0x%x %d %d\n",
+				  vendor, EDID_PRODUCT_ID(edid), block, csum);
+			return true;
+		}
+	}
+
+	DRM_DEBUG("No checksum quirk for %s 0x%x %d %d\n", vendor,
+		  EDID_PRODUCT_ID(edid), block, csum);
+	return false;
+}
+
 /**
  * drm_edid_block_valid - Sanity check the EDID block (base or extension)
  * @raw_edid: pointer to raw EDID block
@@ -1712,6 +1758,9 @@ bool drm_edid_block_valid(u8 *raw_edid, int block, bool print_bad_edid,
 			DRM_DEBUG("EDID checksum is invalid, remainder is %d\n", csum);
 			DRM_DEBUG("Assuming a KVM switch modified the CEA block but left the original checksum\n");
 		} else {
+			if (checksum_quirk (edid, 0, csum))
+				return true;
+
 			if (print_bad_edid)
 				DRM_NOTE("EDID checksum is invalid, remainder is %d\n", csum);
 
